# -*- coding: utf-8 -*-

"""

Module for simple SDPR test GUI.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

(c) 2019 Korhan Kanar

"""

import tkinter

import numpy as np
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib.figure import Figure

from sdpr import Classifier
from .utils import mov_avg

class TesterGui():
    """
    Graphical user interface for editing time-series
    and testing with SDPR.
    """
    
    def __init__(self, y=None, classifier=None, showpassing=False):
        """
        Constructor for the TesterGui object.
        
        Parameters:
        -----------
        y : numpy.array (optional)
            Array to be manipulated.
            If not specified or set as None, a linear growth 
            array from 0 to 1 with 120 elements is created.
        classifier : sdpr.Classifier (optional)
            If not specified, deafult SDPR classifier is used.
        showpassing : boolean (optional)
            If True, also lists passing classes. 
            (default is False)
            
        Examples
        -------_
        >>> from sdpr.utils import TesterGui
        >>> gui = TesterGui()
        
        GUI window is initalized with the default sample, which is 
        a linearly growing trend from 0.0 to 1.0 with 120 points.
        
        Alternatively, the application with can be initiated with 
        a time-series sample that is created in the Python environment:

        >>> import numpy as np
        >>> t = np.arange(120)
        >>> arr = np.exp(-t/30)
        >>> gui = TesterGui(y=arr)
        
        Arrays can be saved and retrieved as numpy .npy files 
        from the File menu. After closing the window, created array
        can be reach from the Python environment using
        
        >>> gui.array()
        
        Files saved from the GUI are .npy files which can be 
        retrieved using numpy.load(file_name) command.
        
        Curves may be smoothed using moving averages. 
        Smoothing factor, k, is the number of points to the 
        left and right of any point, thus window size becomes 2*k+1. 
        Smoothing is implemented in util.mov_avg() function
        
        """
                
        # Create TK root
        self._root = tkinter.Tk()
        self._root.wm_title("SDPR Tester")
          
        self._menubar = tkinter.Menu(self._root)
        self._filemenu = tkinter.Menu(self._menubar, tearoff=0)
        self._filemenu.add_command(label="Load Data...",
                                  command=self._loadData)
        
        self._filemenu.add_command(label="Save Data As...",
                                  command=self._saveDataAs)
        
        self._menubar.add_cascade(label="File", menu=self._filemenu)
        self._root.config(menu=self._menubar)
        
        if y is not None:
            self._set_array(y)
        else:
            self._set_array(np.linspace(0,1,120))
            
        self._pressed = False
        
        self._fig = Figure(figsize=(5.6, 3), dpi=100)
 
        self._fig.add_subplot(111).plot(self._t, self._y)
        self._fig.gca().grid()
        # A tk.DrawingArea
        self._canvas = FigureCanvasTkAgg(self._fig, 
                                        master=self._root)
                                        
        self._canvas.draw()
        
        self._canvas.get_tk_widget().grid(row=0, 
                                         columnspan=4,
                                         rowspan=4,
                                         pady=5)
        
        self._line = self._fig.gca().get_lines()[0]
        
        self._canvas.mpl_connect('button_press_event', self._mouse_press)
        self._canvas.mpl_connect('button_release_event', self._mouse_release)
        self._canvas.mpl_connect('motion_notify_event', self._mouse_move)
 
        # Text widget -> test result display                
        self._text = tkinter.Text(self._root, width=80, height=8)
        self._text.grid(row=4, columnspan=4, rowspan=2, padx=10)
        self._text.config(spacing3=4)
              
        self._bestText = tkinter.Text(self._root, width=80, height=2)
        self._bestText.grid(row=8, columnspan=4, rowspan=1)
        self._bestText.config(spacing3=4)
            
        # Buttons
        self._bCanvas = tkinter.Canvas(master=self._root)
        
        self._lf_smooth = tkinter.LabelFrame(master=self._bCanvas,
                                     text="Smoothing\nfactor")
        self._lf_smooth.pack(padx=10, pady=10)
        
        self._scale_smooth = tkinter.Scale(self._lf_smooth, from_=1, to=10)
        self._scale_smooth.pack(pady=5)
        
        self._b_smooth = tkinter.Button(master=self._lf_smooth, 
                                     text="Smooth", command=self._smooth)
 
        self._b_smooth.pack(padx=5, pady=10)
        
        self._b_test = tkinter.Button(master=self._bCanvas,
                             text="Classify ", command=self._test)
        self._b_test.pack()
        
        self._bCanvas.grid(row=0, column=4, rowspan=3)
        
        # Classifier
        if classifier == None:
            self._classifier = Classifier.create()
        else:
            self._classifier = classifier
            
        self._showpassing = showpassing
        tkinter.mainloop()
        
    def _set_array(self, y):
        self._y = y.copy()
        self._t = np.arange(len(y))
        
    def array(self):
        """
        Return data array.
        """
        return self._y
    
    def _loadData(self):
        file = tkinter.filedialog.askopenfile(mode="r")
        if file is not None:
            y = np.load(file.name)
            self._set_array(y)
            self._updateCurve()
    
    def _saveDataAs(self):
        file = tkinter.filedialog.asksaveasfile(
                mode='w',
                defaultextension=".npy")
        if file is not None:
            file.close()
            np.save(file.name, self._y)
           
    def _mouse_press(self, event):
        self._pressed = True        
        
    def _mouse_release(self, event):
        self._pressed = False        
        
    def _mouse_move(self, event):
        if self._pressed:
            ix, iy = float(event.xdata), float(event.ydata)
            ind = np.argmin(np.abs(ix-self._t))
        
            self._y[ind] = iy
            self._updateCurve()
                           
    def _smooth(self):
        self._y = mov_avg(self._y, self._scale_smooth.get())
        self._updateCurve()
        
    def _updateCurve(self):
        self._line.set_data(self._t, self._y)
        self._canvas.draw() 
            
    def _test(self):
        table = self._classifier.calc_scores(self._y)
       
        self._text.delete(1.0, tkinter.END)
        self._text.insert(tkinter.INSERT, str(table))
        
        bestStr = "Best score : {0} ({1:.2f})\n".format(
                table.best().name,
                table.score(table.best()))
        
        if self._showpassing:
            bestStr += "Passing    : "
            for p in table.passing():
                bestStr += p.name + " "
        
        self._bestText.delete(1.0, tkinter.END)
        self._bestText.insert(tkinter.INSERT, bestStr)