# -*- coding: utf-8 -*-
"""
Utility functions for SDPR.

(c) 2019 Korhan Kanar 

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""

import numpy as np


def mov_avg(y, k):
    """
    Smooth by moving averages.
    
    Parameters
    ----------
    y : numpy.array
        Array to be smoothed
    k : int
        Smoothing factor.
        Each value in the smoothed array is found by averaging 
        2*k+1 points in y. (k points left, k points right and itself)
        Close to the ends, only available number of points are used.
    Returns
    -------
    numpy.array
        Smoothed array.
    """
    
    N = len(y)
    ind = np.arange(N)
    d = np.minimum(np.minimum(k, ind), 
                   np.minimum(N-ind-1, k))

    ys = np.zeros_like(y)
    for i in range(N):
        ys[i] = np.mean(y[i-d[i]:i+d[i]+1])
    return ys

