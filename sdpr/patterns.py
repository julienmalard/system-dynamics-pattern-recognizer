
"""
Pattern definitions and functions for System Dynamics 
Pattern Recognizer.

(c) 2019 Korhan Kanar

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""

import os
import pkg_resources
from enum import IntEnum

import matplotlib.pyplot as plt
import matplotlib.image as mpimg


class Pattern(IntEnum):
    """
    Enumeration for dynamic pattern classes.
    
    Values are the same as the id's in ISTS-99 version. However, 'none' 
    pattern is added for the "None of the defined classes" case, with 
    the value = 0.
    (Matlab array indices start from 1, whereas in Python they start 
    from 0)
    
    """
    NONE = 0
    ZERO = 1
    CONST = 2
    PLINR = 3
    NLINR = 4
    NEXGR = 5
    SSHGR = 6
    PEXGR = 7
    GR1DA = 8
    GR1DB = 9
    GR2DA = 10
    GR2DB = 11
    D1PEG = 12
    D2PEG = 13
    NEXDC = 14
    SSHDC = 15
    PEXDC = 16
    D1GRA = 17
    D1GRB = 18
    D2GRA = 19
    D2GRB = 20
    G1PED = 21
    G2PED = 22
    OSCCT = 23
    OSCGR = 24
    OSCDC = 25
    
    
# Descriptions for the patterns
_DESCRIPTIONS = [    
    "None of the predefined patterns",
    "Constant zero value",
    "Constant",
    "Linear with positive slope",
    "Linear with negative slope",
    "Negative exponential growth",
    "S-shaped growth",
    "Positive exponential growth",
    "Growth with decreasing rate followed by decline to\n"
    + "equilibrium (growth level is less than decline level)",
    "Growth with decreasing rate followed by decline to\n"
    + "equilibrium (growth level is greater than decline level)",
    "S-shaped growth and decline to equilibrium (growth\n"
    + "level is less than decline level)",
    "S-shaped exponential growth and decline to "
    + "equilibrium (growth level is greater than decline level)",
    "Decline with increasing rate followed by positive exponential growth",
    "S-shaped decline followed by positive exponential decline",
    "Negative exponential decline",
    "S-shaped decline",
    "Positive exponential decline",
    "Decline with increasing rate followed by growth to\n"
    + "equilibrium (decline level is less than growth level)",
    "Decline with decreasing rate followed by decline to\n"
    + "equilibrium (growth level is less than decline level)",
    "S-shaped decline and growth to equilibrium (decline\n"
    + "level is less than growth level)",
    "S-shaped decline and growth to equilibrium (decline\n"
    + "level is greater than growth level)",
    "Decline with decreasing rate followed by positive\n"
    + "exponential decline",
    "S-shaped growth followed by positive exponential decline",
    "Oscillation around constant mean",
    "Oscillation around linearly growing trend",
    "Oscillation around linearly declining trend"
    ]
        
                         
def pattern_info(arg):
    """
    Print name, ID and description information for any of
    the predefined patterns.
    
    Parameters
    ----------
    arg : int, string, Pattern enumeration
        ID number, name or Pattern enumeration of the pattern
        for which information is required.
    """
    if type(arg) == str:
        pid = Pattern[arg]
    else:
        pid = arg
    print("Pattern ID  :  {0}".format(pid))
    print("Pattern Name: ", Pattern(pid).name)
    print("Description :")
    print(_DESCRIPTIONS[pid])        

        
def display_all():
    """
    Display shapes of the predefined patterns on
    a single image in a new figure window.
    """ 
    IMG_DIR_PATH = pkg_resources.resource_filename('sdpr', 'img')
    imgFileName = os.path.join(IMG_DIR_PATH, 'patterns.png')    

    img = mpimg.imread(imgFileName)
    plt.figure(figsize = (12,12)) 
    plt.imshow(img, interpolation="bilinear")
