#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Python module for System Dynamics behaviour classification.

Algorithms are based on
 
Kanar, K., 1999, “Structure Oriented Behavior Tests in 
Model Validation,” Boğaziçi University 
Institute for Graduate Studies in Science and Engineering, 
Industrial Engineering Department M.S. Thesis. 

(c) 2019 Korhan Kanar

"""
__version__ = '1.1.5'

from .patterns import *
from .core import *

