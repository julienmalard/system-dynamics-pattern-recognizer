"""
Core functions and classes for System Dynamics Pattern 
Recognizer.

(c) 2019 Korhan Kanar

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""

import os
import json
import pkg_resources
from enum import IntEnum

import numpy as np

from sdpr.patterns import Pattern

    
class HMM_Model:
    """
    HMM Model class.            
    """
    def __init__(self, classID, T, N, p, A, m, V, mean_lkd,
                           std_lkd, mean_lkd_terms):
        """
        Constructor for HMM_Model object.
        """
        self.classID = classID
        self.T = T
        self.N = N
        self.p = p
        self.A = A
        self.m = m
        self.V = V
        self._mean_lkd = mean_lkd
        self._std_lkd = std_lkd
        self._mean_lkd_terms = mean_lkd_terms
 
                
    def likelihood(self, feat):
        lkd, optseq, lkd_terms = _st_opt_lkd(
            self.p, self.A, self.m, 
            self.V, feat)
        normLkd = (lkd-self._mean_lkd) / self._std_lkd
        return {'normLkd':normLkd, 
               'lkd':lkd, 
               'lkd_terms':lkd_terms, 
               'optseq':optseq}

    def dataDict(self):
        """
        Get model paramters in a dictionary.
        Returns
        -------
        dict
            Dictionary with keys "classID", "T", "N", "p", "A", 
            "m", "V", "source", "lkds_mod"
        """
        
        model = {"classID": self.classID,                 
                 "T" : self.T,
                 "N" : self.N,
                 "p" : self.p.tolist(),
                 "A" : self.A.tolist(),
                 "m" :self.m.tolist(),
                 "V" : self.V.tolist(),
                 "mean_lkd" : self._mean_lkd,
                 "std_lkd" : self._std_lkd,
                 "mean_lkd_terms" : self._mean_lkd_terms.tolist() }
        return model
                      

class ScoreTable:
    """
    Table that stores and manipulates classification scores
    for the give time-series.
    """  
    
    def __init__(self, scores, minScore):
        """
        Constructor for ScoreTable object.
        Parameters
        ----------
        scores : numpy.array
            Array of pattern class scores
            
        minScore : float
            Minimumd score value for classifying as a pattern.
        
        """
        self._scores = scores
        self._MIN_SCORE = minScore
        
    def predicted(self):
        """
        Return predicted class according to the classification rule.
        
        Returns
        -------
        int
            If the maximum score in the table is higher than the 
            minimum score threshold, returns the index of that element. Otherwise,
            retruns 0. 
               
        if self._scores.max() < self._MIN_SCORE:
            return Pattern.NONE
        else:
            return Pattern(self._scores.argmax())
        """ 
        return Pattern(self._indPredicted())
    
    def _indPredicted(self):
        if self._scores.max() < self._MIN_SCORE:
            return 0
        else:
            return self._scores.argmax()
        
    def passing(self):
        """
        Return list of the patterns whose scores are above
        the minimum score value.
        Returns
        -------
        list
            List where elements are the Pattern enumerations.
        """
        indSort = np.argsort(-self._scores)
        indPass = indSort[self._scores[indSort] >= self._MIN_SCORE]            
        return [Pattern(k) for k in indPass]        
    
    def best(self):
        """
        Get the best performing class in the ScoreTable object.
        
        Returns
        -------
        sdpr.Pattern
        """
        
        i = np.argmax(self._scores)
        return Pattern(i)
        
    def score(self, cid):
        """
        Get score for a given pattern class.
        
        Parameter
        ---------
        cid : int or sdpr.Pattern
            ID of the pattern class
        
        Returns
        -------
        numpy.float64
            Score value for the give pattern class
        """
        return self._scores[Pattern(cid)]
        
    def __str__(self):
        """
        Re-implementation of the __str__ function.
        Used when print() is called on the ScoreTable object.
        """
        
        indPredicted = self._indPredicted()
        
        tableStr = ""
        for cid, score in enumerate(self._scores): 
            marker = "*" if cid == indPredicted else " "                                         
            tableStr += "{0:1} {1:5}: {2:6.2f}   ".format(marker, 
                          Pattern(cid).name, self._scores[cid])
            if (cid % 4) == 3:
                tableStr += "\n"  
            
        return tableStr    
                

class NormType(IntEnum):
    """
    Enumeration for normalization type of the time-series.
    """
    Regular = 0
    Reflected = 1
    RegOsc = 2
    OcsNoTrend = 3


class Classifier:
    """
    Classifier class for behaviour patterns.
    """
    
    _MODEL_FNAME = 'models.json'        
    _MIN_SCORE = -3.0   # Minimum score threshold
    _MIN_SLOPE_OSC = 0.15   # Minimum absolute slope for oscillation
    
    _model_id = ""
    
    # Class relationships for HMM Models
    _hmmRels = [None] * len(Pattern)
    _hmmRels[Pattern.NEXGR] = [
        (Pattern.NEXGR, NormType.Regular),
        (Pattern.NEXDC, NormType.Reflected)]
    _hmmRels[Pattern.SSHGR] = [
        (Pattern.SSHGR, NormType.Regular),
        (Pattern.SSHDC, NormType.Reflected)]
    _hmmRels[Pattern.PEXGR] = [
        (Pattern.PEXGR, NormType.Regular),
        (Pattern.PEXDC, NormType.Reflected)]
    _hmmRels[Pattern.GR1DA] = [
        (Pattern.GR1DA, NormType.Regular), 
        (Pattern.D1GRA, NormType.Reflected)]
    _hmmRels[Pattern.GR1DB] = [
        (Pattern.GR1DB, NormType.Regular),
        (Pattern.D1GRB, NormType.Reflected)]
    _hmmRels[Pattern.GR2DA] = [
        (Pattern.GR2DA, NormType.Regular),
        (Pattern.D2GRA, NormType.Reflected)]
    _hmmRels[Pattern.GR2DB] = [
        (Pattern.GR2DB, NormType.Regular), 
        (Pattern.D2GRB, NormType.Reflected)]
    _hmmRels[Pattern.D1PEG] = [
        (Pattern.D1PEG, NormType.Regular), 
        (Pattern.G1PED, NormType.Reflected)]
    _hmmRels[Pattern.D2PEG] = [
        (Pattern.D2PEG, NormType.Regular), 
        (Pattern.G2PED, NormType.Reflected)]
    _hmmRels[Pattern.OSCCT] = [
        (Pattern.OSCCT, NormType.RegOsc), 
        (Pattern.OSCGR, NormType.OcsNoTrend),
        (Pattern.OSCDC, NormType.OcsNoTrend)] 
    
    _hmmRels[Pattern.PLINR] = [
        (Pattern.PLINR, NormType.Regular), 
        (Pattern.NLINR, NormType.Reflected)]
                       
    def __init__(self):
        """
        Constructor for blank Classifier object.
        
        In order to create a classifier with properly constructed
        models, use static function Classifier.create()
        
        """
        self._models = []
               
    @staticmethod    
    def create(fname=_MODEL_FNAME):
        """
        Create HMM_Classifier object from the model file.
        """
        
        MODEL_DIR_PATH = pkg_resources.resource_filename('sdpr', 'mod')        
        modelFileName = os.path.join(MODEL_DIR_PATH, fname)
                                     
        
        with open(modelFileName,'r') as f:
            d = json.load(f)
        
        classifier = Classifier()
        classifier._model_id = d["model_id"]
        for modelDict in d["models"]:
            
            newModel = HMM_Model(modelDict["classID"],
                                 modelDict["T"],
                                 modelDict["N"],
                                 np.array(modelDict["p"]),
                                 np.array(modelDict["A"]),
                                 np.array(modelDict["m"]),
                                 np.array(modelDict["V"]),
                                 modelDict["mean_lkd"],
                                 modelDict["std_lkd"],
                                 np.array(modelDict["mean_lkd_terms"])
                                 )
            classifier.addModel(newModel)
        return classifier
    
    def get_model_id(self):
        return self._model_id
    
    def addModel(self, newModel):
        self._models.append(newModel)
    
    def predict(self, arr):
        """
        Predict the behavior class of the give time-series.
        
        Parameters
        ----------
        arr : numpy.array
            Time-series array to be classifed
            
        Returns
        -------
        pred : Pattern 
            Predicted pattern.
            
        Example
        --------
        >>> import sdpr
        >>> classifier = sdpr.Classifier.create()
        >>> import numpy as np
        >>> t = np.linspace(0.0, 1.0, 120)
        >>> y = np.exp(-t/0.2)
        >>> pred = classifier.predict(y)
        >>> pred.value
        14
        >>> pred.name
        >>> 'NEXDC'
        sdpr.pattern_info(pred)
        Pattern ID  :  14
        Pattern Name:  NEXDC
        Description :
        Negative exponential decline
        
        """
        table = self.calc_scores(arr)
        pred = table.predicted()
        return pred
                
    def calc_scores(self, arr):
        """
        Form classification table containing likelihood scores
        for the given time-series.
        
        Parameters
        ----------
        arr : numpy.array
            Time-series array to be classifed
            
        Returns
        -------
        ScoreTable
            ScoreTable object
        """
        
        T = self._models[0].T       
        scores = np.zeros(len(Pattern)) - 99.0
        
        # Preprocess input
        try:
            f, slope_osc = _preprocess(arr, T)
        except ConstantException as ce:
            if ce.value == 0.0:
                scores[Pattern.ZERO] = 3.0
            else:
                scores[Pattern.CONST] = 3.0
            return ScoreTable(scores, self._MIN_SCORE)
        
        # Test for constant pattern
        if _isInConstBand(arr):
            # TODO : 
            scores[Pattern.CONST] = -2.99
                
        # Scores according to the HMM models
        for model in self._models:
            testList = self._hmmRels[model.classID]
            for test in testList:
                cid, normIndex = test  
                scores[cid] = model.likelihood(f[normIndex])['normLkd']                
                
                # Slope checks for OSCGR and OSCDC classes
                if cid == Pattern.OSCGR and slope_osc < self._MIN_SLOPE_OSC:
                    scores[cid] = -99.0
                elif cid == Pattern.OSCDC and slope_osc > -self._MIN_SLOPE_OSC:
                    scores[cid] = -99.0                
        return ScoreTable(scores, self._MIN_SCORE)                


def _feat1D(x,t):
    """
    Feature extraction for a single array.
    
    Parameters
    ----------
    x : numpy.array
        Time-series data array
    t : int
        Number of segments
        
    Returns
    -------
    numpy.array
        3xt sized array of feature vectors
    
    """
    f = np.zeros((3, t))
    N = len(x)
    pointsLeft = N
    sbeg = 0
    segLeft = t
    while segLeft > 0:
        segLength = int(np.round(pointsLeft/segLeft))
        send = sbeg + segLength
        
        # First-order fitting
        p1 = _pfit1(x[sbeg:send], 1, N)
        f[0, t-segLeft] = p1[0]       # 1st feature : slope
            
        p2 = _pfit1(x[sbeg:send], 2, N)
            
        rmid = 0.5*(segLength-1)/(N-1)
        a1 = p2[1]
        a2 = p2[0]
        par = a1**2 + 4*a1*a2*rmid + 4*a2**2*rmid**2;
        
        f[1, t-segLeft] = 2*a2/(1+par)**1.5;
        f[2, t-segLeft] = np.mean(x[sbeg:send])
        
        # Updates for next segment
        pointsLeft -= segLength 
        sbeg = send
        segLeft -= 1
    return f

def _feat(X,t):
    """
    Extract features.
    
    Parameters
    ----------
    X : {np.array}
        1-D or 2-D numpy array. If 2-D matrix is given, 
        each row is treated as a separate array.
   
    t : {int}
        Number of segments
    
    Returns
    -------
    {np.array}
        2-D Numpy array of features. For 2-D input case,
        arrays of feature vectors are concatenated to form
        a 3x(nrows*t) sized array, where nrows is the number
        of rows in X.
    """   
    shape = X.shape
    if len(shape) == 1:
        return _feat1D(X,t)
    else:
        f = np.zeros((3,t*shape[0]))
        i = 0
        for x in X:
            f[:, i:i+t] = _feat1D(x,t)
            i += t
        return f


def _st_opt_lkd(p, A, m, V, S):
    """
    Find state-optimized likelihood values using
    the Viterbi Algorithm.
    
    Parameters
    ----------
    p : numpy.array
        Initial probability vector
    A : numpy.array
        State-transition matrices between segments.
    m : numpy.array
        Feature means for each state.
    V : numpy.array
        Covariance matrices of feature vectors for each state.
    S : numpy.array
        3xT sized array of feature vectors, where T is the 
        number of segments.
        
    Returns
    -------
    lkd :
        State-optimized likelihood value (logarithmic)
    optseq :
        Optimal state sequence
    lkd_terms :
    """
    N, _ = A.shape
    rS, T = S.shape

    # State numbers are between 0 and N-1
    b = np.zeros((N,T))
    for i in range(N):
        C = V[:, i*rS:(i+1)*rS]
        cons = 1 / ((2*np.pi)**rS * (np.linalg.det(C)**0.5))
        for j in range(T):
            omm = np.reshape(S[:,j] - m[:,i], (rS,1))

            par = -0.5 * float(np.dot(np.dot(omm.T, np.linalg.inv(C)),
                                      omm))
            b[i,j] = cons * np.exp(par)

    # Logaritmic conversion
    pl = np.log(np.maximum(p, 1.0E-10))
    Al = np.log(np.maximum(A, 1.0E-10))
    bl = np.log(np.maximum(b, 1.0E-10))

    d = np.zeros((T,N))

    d[0,:] = pl.T + bl[:,0].T
    psi = np.zeros((T,N), dtype=np.int)
    #mat = np.zeros(((T-1)*N,1))
    for t in range(1,T):    # for t=2:T
        for j in range(N):
            dd = d[t-1,:] + Al[:,j+(t-1)*N].T + bl[j,t]
            d[t,j] = np.max(dd)
            argmax = np.argmax(dd)
            psi[t,j] = argmax

    # *** TERMINATION ***
    lkd = np.max(d[T-1,:])
    optseq = [0]*T
    optseq[T-1] = np.argmax(d[T-1, :])

    for t in range(T-2, -1, -1):
        optseq[t] = psi[t+1, optseq[t+1]]
        
    # Terms of the likelihood value ----------
    lkd_terms = np.zeros(2*T)
    S = 0
    k = optseq[0]
    S += pl[k] + bl[k,0]
    lkd_terms[0] = pl[k]
    lkd_terms[1] = bl[k,0]
    for t in range(1,T):
        j = k = optseq[t-1]
        k = optseq[t]
        S += Al[j, (t-1)*N+k]
        S += bl[k,t]
        lkd_terms[t*2] = Al[j, (t-1)*N+k]
        lkd_terms[t*2+1] = bl[k,t]        
    # --------------------------
    return lkd, optseq, lkd_terms


def _isOutOfBand(y, sp, lp):
    """
    Check if any of the intermediate points in an array deviates
    more than a given threshold from the linear line drawn between 
    two given points.
    
    Parameters
    ----------
    y : numpy.array

    sp : int 
        Starting point index
    lp : int
          Last point index (lp > sp+1)
    
    Ouput:
    ------
    result : {Boolean} If there exist at least one point
             that deviates more than the given threshold value.
    """
    THRESHOLD = 0.02

    ysp = y[sp]
    slope = (y[lp]-y[sp]) / (lp-sp)
    yline = slope*np.arange(1,lp-sp) + ysp
    delta = np.abs(yline - y[sp+1:lp])
    
    if any(delta > THRESHOLD):
        return True
    else:
        return False 


def _findPeakInterval(y):
    """
    """
    N = len(y)
    i_start = None
    i_end = None
    
    def updateInterval(slope, seg_beg, seg_end, i_start, i_end):
        if slope > 0:
            i_start = seg_beg
        elif slope < 0 and i_start != None:
            i_end = seg_end
        return (i_start, i_end)
    
    sp = 0
    while sp < N-2:
        lp = sp+2
        # Loop of increasing lp
        while lp < N:
            if _isOutOfBand(y, sp, lp):    # If any interm. point is out of band
                # add previous point as segment end
                slope = (y[lp-1]-y[sp]) / (lp-1-sp)
                i_start, i_end = updateInterval(slope,
                                                sp, 
                                                lp-1, 
                                                i_start, 
                                                i_end)
                sp = lp-1;
                break
            elif lp == N-1:     # If within limits until end
                # add segment 
                slope = (y[N-1]-y[sp]) / (N-1-sp)
                i_start, i_end = updateInterval(slope, 
                                                sp, 
                                                N-1, 
                                                i_start, 
                                                i_end)
                sp = lp;
                break
            else:
                lp +=1
        
        if i_end != None:
            break
        
    return (i_start, i_end)


def _fpeak(y):
    stpt, endpt = _findPeakInterval(y)
    if stpt == None or endpt == None:
        return None
    
    step = 1
    peakCount = 0

    while step < endpt-stpt and peakCount != 1:
        cpt = np.arange(stpt, endpt+1, step)
        ycpt = y[cpt]
        dy = np.diff(ycpt, 1)
        peakCount = 0;
        for i in range(len(dy)-1):
            if dy[i] > 0 and dy[i+1] < 0:
                p_found = cpt[i]+1;
                peakCount += 1
        step +=1

    return p_found


def chsize(y, M):
    """
    Change size of an array by creating points
    using Lagrangian interpolation.
    """
    N = len(y)
    x = np.arange(M)
    x1 = (np.floor((N-1)*x/(M-1))-1).astype(np.int32)
    x1 = np.maximum(x1, np.zeros_like(x))
    x1 = np.minimum(x1, np.zeros_like(x)+ N-4)

    # Lagrangian interpolation wih 4 points
    x0 = x *(N-1) / (M-1)
    y0 = y[x1]*(x0-x1-1)*(x0-x1-2)*(x0-x1-3)/(-6) \
        + y[x1+1]*(x0-x1)*(x0-x1-2)*(x0-x1-3)/2 \
        + y[x1+2]*(x0-x1)*(x0-x1-1)*(x0-x1-3)/(-2) \
        + y[x1+3]*(x0-x1)*(x0-x1-1)*(x0-x1-2)/6
    
    return y0


class ConstantException(Exception):
    """ Exception raised when all values in the array turn out 
    to have the same value during normalization.
    Value is the constant value
    """
    def __init__(self, value):
        self.value = value
        
    def __str__(self):
        return repr(self.value)


def _autocf(x):
    """
    Calculate auto-correlation function.
    Parameter
    ---------
    x : Numpy array
    
    Returns
    numpy.array
        Autocorrelation function
    
    """
    N = len(x)    
    xm = np.mean(x)
    N2 = N//2
    r = np.zeros(N2)
    for k in range(1, N2+1):
        for i in range(N-k):
            r[k-1] += (x[i]-xm) * (x[i+k]-xm)
    denum = 0
    for i in range(N):
        denum += (x[i]-xm)**2
    return (r / denum)
    

def _normalize(arr):
    """
    Normalize signal to be classified linearly between 0.0 and 1.0
    
    Parameters
    ----------
    arr : numpy.ndarray
        Input time-series array
        
    Returns
    -------
    numpy.ndarray
        Normalized array 
    
    Raises
    ------
    ConstantException
        when all values have the same value.
    """
    
    arrMin = np.min(arr)
    arrMax = np.max(arr)
    arrRange = arrMax - arrMin
    if arrRange == 0:
        raise ConstantException(arr[0])
    return (arr-arrMin) / arrRange
                
        
def _autocorrFeat(y, T):
    """
    Extract features after the truncating the autocorrelation function.
    """
    y_acf = _autocf(y)    
    peak_ind = _fpeak(y_acf)
    
    if peak_ind != None:
        y_acf_trunc = chsize(y_acf[0:peak_ind+1], len(y))        
        f =  _feat(y_acf_trunc, T)
    else:
        f = np.zeros((3,T))
    return f
 
    
def _preprocess(y, T):
    """
    Preprocess signal for classification
    Parameters
    ----------
    y : numpy array
        1-D array to be classified
    T : int
        Number of segments for HMM
    Returns
    -------
    f : {List}
        List with three elements where each element is
        a sequence of feature vectors (2-D Numpy array)
        f[0] : Features extracted after normalization
        f[1] : Features extracted after normalization of the reflected input
        f[2] : Features after preprocessing for oscillation
               (Autocorrelation function - peak finding - resizing)
    Raises
    ------
    TypeError
        When y is not a NumPy array
    Exception
        When the y is not 1-dimensional (y.ndim must be 1),
        Or when the length of the input array is less than 60.
            
    """
    
    # Checks for input type, dimension and length
    if type(y) != np.ndarray:
        raise TypeError("Input must be a NumPy array")
    
    if y.ndim != 1:
        raise Exception("Dimension of the input array must be 1.")
        
    if len(y) < 60:
        raise Exception("Length of input array must be at least 60")
        
    yN = _normalize(y)
    f0 = _feat(yN, T)   
    f1 = _feat(1.0-yN, T)  
    f2 = _autocorrFeat(yN, T)  
    # ------------------------
    coefs = _pfit1(yN, 1, len(yN))   
    t = np.linspace(0, 1, len(yN))
    ylf = np.polyval([coefs[0], coefs[1]], t)
    #plt.plot(t,yN,t,ylf)
    
    f3 = _autocorrFeat(_normalize(yN-ylf), T)
    
    return [f0, f1, f2, f3], coefs[0]
    

def _isInConstBand(y):
    """
    Check if the values in the array are in a constant
    band around the mean mean.
    
    Parameters
    ----------
    y : numpy.array
        Tİme-series data
        
    Returns
    -------
    bool
        True if within band, False otherwise.
    """
    m = np.mean(y)
    upper_limit = 1.05*m
    lower_limit = 0.95*m

    if np.any((y < lower_limit) | (y > upper_limit)):
        return False
    else:
        return True

    
def _pfit1(y, order, N):
    """
    Fit polynomial with the implied independent axis points.
    
    The x-coordinates of the points start with 0.0 and the increment
    depends on parameter, N. If N is equal to length of y, independent 
    axis points are equally spaced between 0.0 and 1.0.
    N is typically greater than the array length, which leads to 
    x-coordinates from 0.0 to a number which is less than 1.0.
    
    Parameters
    ----------
    y : numpy.array
        Y-axis values of the points
    order : int
        Order of the polynomial
    N : int
        Number of points which lead x-coordinates in range [0.0, 1.0]
        
    Returns
    -------
    numpy.array
        Array of polynomial coefficients.
        
    """
    n = len(y)
    t = np.linspace(0, (n-1)/(N-1), n)
    a = np.polyfit(t, y, order)    
    return a  
